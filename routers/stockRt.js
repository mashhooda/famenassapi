const express = require('express');
const stockRt = express.Router();
const stockCtObj = require('../controller/stockCt');

stockRt.get('/getStockData', stockCtObj.getStockData);

module.exports = stockRt;