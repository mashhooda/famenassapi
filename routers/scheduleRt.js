const express = require('express');
const scheduleRt = express.Router();
const scheduleCtObj = require('../controller/scheduleCt');

scheduleRt.get('/getEmpTodaysSchedules', scheduleCtObj.getEmpTodaysSchedules);
scheduleRt.post('/UpdateJobCard', scheduleCtObj.UpdateJobCard);
scheduleRt.post('/UpdateSignature', scheduleCtObj.UpdateSignature);
scheduleRt.post('/UpdateJobCardWorkSheetAttendance', scheduleCtObj.UpdateJobCardWorkSheetAttendance);

module.exports = scheduleRt;