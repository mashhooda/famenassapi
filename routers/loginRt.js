const express = require('express');
const loginRt = express.Router();
const loginCtObj = require('../controller/loginCt');

loginRt.get('/getLoginData', loginCtObj.getLoginData);

module.exports = loginRt;