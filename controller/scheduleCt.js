const scheduleMd = require('../models/scheduleMd');
const con = require('../utils/connection');
const sqlObj = require('mssql');
const fs = require('fs');
const generalModel = require('../models/generalMd');
const sql=require('mssql');
const resultUtil = require('../utils/errorHandling');

exports.UpdateJobCardWorkSheetAttendance=async(req,res,next)=>{
    let conObj=await con.connect();
    let transactionObj=new sql.Transaction(conObj);
    transactionObj.begin(async (te) => {
try {
   
    let payLoad1=req.body;
    let payLoad=payLoad1['data'];
    let header=payLoad['Attendance'];
  
    let UpdateJobCardWorkSheetAttendance=await scheduleMd.UpdateJobCardWorkSheetAttendance(transactionObj, header);
    let UpdateJobCardHdr=await scheduleMd.updateJobCardHdrServiceHrs(transactionObj, header);
    transactionObj.commit(er => {
        res.send(resultUtil.resultMsg( header['DocNo'], 'updated', false, header['DocNo']));
    })
    
} catch (error) {
    transactionObj.rollback(er => {
        res.send(resultUtil.resultMsg(0, false, error.toString()));
    })
    
}
    });
}
exports.UpdateJobCard=async(req,res,next)=>{
    let conObj=await con.connect();
    let transactionObj=new sql.Transaction(conObj);
    transactionObj.begin(async (te) => {
try {

    let payLoad1=req.body;
    let payLoad=payLoad1['data'];
    let header=payLoad['JobCardData'];
    let DocNo=header['DocNo'];
    let UpdateJobCardHdr=await scheduleMd.updateJobCardHdr(transactionObj, header);
    let UpdateJobCardWorkSheet = await scheduleMd.UpdateJobCardWorkSheet(transactionObj, payLoad);
    let DeleteExistingMaterialInfo = await scheduleMd.DeleteExistingMaterialInfo(transactionObj,header['CompId'],header['SiteId'],header['DocNo']);
    let MaterialInfo=payLoad['MaterialInfo'];
    for (const itemKey in MaterialInfo) {
           
       
        let insertMaterialInfo= await scheduleMd.insertMaterialInfo(transactionObj,MaterialInfo[itemKey]);
    }

    // let sysLogObj = await generalModel.createSysLog(transactionObj, {
    //     SysTerminal: 'Mobile Log', CompId: header['CompId'], SiteId: header['SiteId'],
    //      UserId: header['LastModifiedBy'], LogAction: 'Log from mobile', DocCd: 30524, 
    //      DocKey: DocNo, Description: 'Update JobCard'
    // });
    transactionObj.commit(er => {
        res.send(resultUtil.resultMsg(DocNo, 'inserted', false, DocNo));
    })
    
} catch (error) {
    transactionObj.rollback(er => {
        res.send(resultUtil.resultMsg(0, false, error.toString()));
    })
    
}
    });
}

exports.getEmpTodaysSchedules= async (req, res, next) => {

    try {
       
        let SchedulesData = await scheduleMd.getEmpTodaysSchedules(req.query.EmpCd,req.query.fromDt,req.query.toDt);

        for (let i in SchedulesData) {
                
            SchedulesData[i]['TeamMembers'] = await scheduleMd.getTeamMembers(
                 SchedulesData[i]['CompId'],SchedulesData[i]['SiteId'],
                 SchedulesData[i]['DocCd'],SchedulesData[i]['DocNo'],
                 SchedulesData[i]['Revision']);

            SchedulesData[i]['GetMaterialInfo'] = await scheduleMd.getMaterialInfo(
                    SchedulesData[i]['CompId'],SchedulesData[i]['SiteId'],
                    SchedulesData[i]['DocCd'],SchedulesData[i]['DocNo'],
                    SchedulesData[i]['Revision']);    

                    SchedulesData[i]['GetTechnicalDiagnostics'] = await scheduleMd.getTechnicalDiagnostics(
                        SchedulesData[i]['CompId'],SchedulesData[i]['SiteId'],
                        SchedulesData[i]['DocCd'],SchedulesData[i]['DocNo'],
                        SchedulesData[i]['Revision']);      

                       
        }
        
        res.send({
            
            'SchedulesData': (SchedulesData.length == 0) ? [] : SchedulesData
        });
    }
    catch (e) {
        res.send({
            status: false,
            data: [],
            reason: e.toString(),
        });
    }
}

exports.UpdateSignature = async (req, res, next) => {
    try {
        let payLoad=req.body;
       
        let header=payLoad['Schedule'];
  
    
        var data =  header['sign'];
        var matches = data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
       
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      
        var imageBuffer =new Buffer(matches[2], 'base64');
        fs.writeFile('./images/result3.png', imageBuffer, function(err) {
            if(err) {
                return console.log(err);
            }
        
            console.log("The file was saved!");
         });
        console.log(imageBuffer);

        res.send({
            'Result':'Success'
        });

    } catch (error) {
        res.send(
            {
                status: false,
                data: [],
                reason: error.toString(),
            }
        );
    }
}
