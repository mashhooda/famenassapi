const loginMd = require('../models/loginMd');
const con = require('../utils/connection');
const sqlObj = require('mssql');

exports.getLoginData = async (req, res, next) => {

    try {
        let payLoad = req.query;
        let connectionObj = await con.connect();
      
        console.log(payLoad);
        let [
            ValidateUser
        ]
            = await Promise.all([
                loginMd.ValidateUser(connectionObj, payLoad)
       
              
            ]);      
        

        res.send({
            
            'ValidateUser': (ValidateUser.length == 0) ? [] : ValidateUser[0]
        });
    }
    catch (e) {
        res.send({
            status: false,
            data: [],
            reason: e.toString(),
        });
    }
}