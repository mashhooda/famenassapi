const stockMd = require('../models/stockMd');
const con = require('../utils/connection');
const sqlObj = require('mssql');

exports.getStockData = async (req, res, next) => {

    try {
      
            
            let StockData = await stockMd.getStockData(req.query.StockCd,req.query.CompId);
        
        res.send({
            
            'StockData': (StockData.length == 0) ? [] : StockData
        });
    }
    catch (e) {
        res.send({
            status: false,
            data: [],
            reason: e.toString(),
        });
    }
}