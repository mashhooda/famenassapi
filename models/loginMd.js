const con = require('../utils/connection');
const squel = require('squel');
const sqlObj = require('mssql');

exports.ValidateUser = (transactionObj, payLoad) => {

    return new Promise(async (resolve, reject) => {

        try {
            const request = new sqlObj.Request(transactionObj);
            request
                .input('P_UserId', sqlObj.VarChar(10), payLoad['userId'])
                .input('P_Password', sqlObj.VarChar(200), payLoad['password'])
                .execute('[Usp_ValidateUserMobile]', (err, result) => {
                    if (!err) {
                        console.log('[Usp_ValidateUserMobile] ', result.recordset);
                        resolve(result.recordset);
                    } else {
                        console.error('[Usp_ValidateUserMobile]  log error ->', err);
                        reject(err);
                    }
                });
        }
        catch (error) {
            reject(error);
        }
    });
};

