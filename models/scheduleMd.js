
const connectionObj = require('../utils/connection');
const squel = require('squel');
const sqlObj = require('mssql');
const CurrTime= new Date().toISOString()
.replace(/\.[\w\W]+?$/, '') // Delete from dot to end.
.replace(/\:|\s|T/g, '-');  // Replace colons, spaces, and T with hyphen.

//const CurrTime=new Date().getTime() ;
const fs = require('fs');

exports.updateJobCardHdrServiceHrs = (transactionObj, payLoad) => {
    return new Promise(async (resolve, reject) => {
        try {
   

            let connect = new sqlObj.Request(transactionObj);
         

            let data = await connect.query(`update WSJobOrderHdr set ServiceHrs=(select sum(Duration) from WSJobOrderWorksheetDet where
            CompId='`+payLoad['CompId']+`' AND    SiteId  ='`+payLoad['SiteId']+`'  AND DocCd=`+payLoad['DocCd']+
            `  AND    DocNo  =`+payLoad['DocNo']+`  
            AND    Revision  =`+payLoad['Revision']+` )
            where  CompId='`+payLoad['CompId']+`' AND    SiteId  ='`+payLoad['SiteId']+`'  AND DocCd=`
            +payLoad['DocCd']+`  AND    DocNo  =`+payLoad['DocNo']+`  
            AND    Revision  =`+payLoad['Revision']);



          

            resolve(data.recordset);
        }
        catch (error) {
            reject(error);
        }
    });
};
exports.UpdateJobCardWorkSheetAttendance = (transactionObj, payLoad) => {
    return new Promise(async (resolve, reject) => {
        try {
            
            var query = squel.update()
                .table('WSJobOrderWorksheetDet')
                .set("InTime", payLoad['InTime'])
                .set("OutTime", payLoad['OutTime'])
                .set("Duration", payLoad['Duration'])
                .where(`CompId = '` + payLoad['CompId'] + `' AND SiteId= '` + payLoad['SiteId'] +`' AND DocCd = ` + 
                payLoad['DocCd'] + ` AND DocNo= ` + payLoad['DocNo'] +` AND  Revision= ` + payLoad['Revision']+` AND  ItemNo= ` + payLoad['ItemNo'] )
                .toString()

            let connect = new sqlObj.Request(transactionObj);
            let data = await connect.query(query);
            resolve(data.recordset);
        }
        catch (error) {
            reject(error);
        }
    });
};

exports.updateJobCardHdr = (transactionObj, payLoad) => {
    return new Promise(async (resolve, reject) => {
        try {
            var query = squel.update()
                .table('WSJobOrderHdr')
                .set("GrossTot", payLoad['GrossTot'])
                .set("DiscountAmt", payLoad['DiscountAmt'])
                .set("DiscountPerc", payLoad['DiscountPerc'])
                .set("TotalAmount", payLoad['TotalAmount'])
                .set("JobCompleted", payLoad['JobCompleted'])
                .set("CompletedDate", payLoad['CompletedDate'])
                .set("JobStatus", payLoad['JobStatus'])
                .where(`CompId = '` + payLoad['CompId'] + `' AND SiteId= '` + payLoad['SiteId'] +`' AND DocCd = ` + 
                payLoad['DocCd'] + ` AND DocNo= ` + payLoad['DocNo'] +` AND  Revision= ` + payLoad['Revision'] )
                .toString()
console.log(query);
            let connect = new sqlObj.Request(transactionObj);
            let data = await connect.query(query);
            resolve(data.recordset);
        }
        catch (error) {
            reject(error);
        }
    });
};

exports.insertMaterialInfo= async (transactionObj, payLoad) => {
    return new Promise(async (resolve, reject) => {
        try {

    
            const request = new sqlObj.Request(transactionObj);
            request
            .input('DocNo', sqlObj.BigInt, payLoad['DocNo'])
            .input('SiteId', sqlObj.VarChar(10), payLoad['SiteId'])
            .input('DocCd', sqlObj.Int, payLoad['DocCd'])
            .input('CompId', sqlObj.VarChar(3), payLoad['CompId'])
            .input('Revision', sqlObj.Int, payLoad['Revision'])
            .input('ItemNo', sqlObj.Int,payLoad['ItemNo'] )
            .input('Code', sqlObj.VarChar(30), payLoad['Code'])
            .input('Type', sqlObj.VarChar(20), payLoad['Type'])
            .input('Description', sqlObj.VarChar(4000), payLoad['Description'])
            .input('Unit', sqlObj.VarChar(20), payLoad['Unit'])
            .input('Qty', sqlObj.Int, payLoad['Qty'])
            .input('Price', sqlObj.Money,payLoad['Price'])
            .input('DiscPer', sqlObj.Money,payLoad['DiscPer'])
            .input('DiscAmt', sqlObj.Money,payLoad['DiscAmt'])
            .input('TaxAmount', sqlObj.Money,payLoad['TaxAmount'])
            .input('TotalPrice', sqlObj.Money,payLoad['TotalPrice'])
            .input('TaxCd', sqlObj.VarChar(20), payLoad['TaxCd'])
            .input('TaxDs', sqlObj.VarChar(50), payLoad['TaxDs'])
                .execute('InsertWSJobOrderEstDet', (err, result) => {
                    if (!err) {
                        console.log('Insert WSJobOrderEstDet', result.recordset);
                        resolve(result.recordset);
                    } else {
                        console.error('WSJobOrderEstDet log error ' + payLoad['DocNo']  + payLoad['ItemNo']+ '->', err);
                        reject(err);
                    }
                });
        }
        catch (error) {
            reject(error);
        }
    });
};

exports.DeleteExistingMaterialInfo = async (transactionObj, CompId, SiteId, DocNo) => {
    return new Promise(async (resolve, reject) => {
        try {
            let query = squel.delete()
                .from('WSJobOrderEstDet')
                .where(`CompId = '` + CompId + `'`)
                .where(`SiteId = '` + SiteId + `'`)
                .where(`DocNo = ` + DocNo + ``)
                .where(`DocCd = 30524`)
                .toString();
            let connect = new sqlObj.Request(transactionObj);
            let deleteObj = await connect.query(query);
            resolve(deleteObj.recordset);
        }
        catch (error) {
            reject(error);
        }
    });
};
exports.UpdateJobCardWorkSheet = async (transactionObj, payLoad) => {
    return new Promise(async (resolve, reject) => {
        try {

       
            let header=payLoad['JobCardData'];
           // console.log(header['CustSign']);
            var CustSignData=header['CustSign'];
            var data =CustSignData;
            var CustSignmatches = data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);           
            var CustSignimageBuffer =new Buffer(CustSignmatches[2], 'base64');
            var CustSignName='Cust_'+header['CustomerCd']+'_'+header['CompId']+'_'+
            '_'+header['SiteId']+'_'+  '_'+header['DocCd'].toString()+'_'+'_'+header['DocNo'].toString()+'_'+
            '_'+header['ItemNo'].toString()+'_'+CurrTime+'.png'
            var Custdir = './Upload/30010/DocNo#'+header['DocNo'].toString()+'/';

            if (!fs.existsSync(Custdir)){
             fs.mkdirSync(Custdir);
             }
            var CustSignPath=Custdir+CustSignName;
         
            if (CustSignmatches.length !== 3) {
                return new Error('Invalid input string');
              }
              fs.writeFile(CustSignPath
              , CustSignimageBuffer, function(err) {
                if(err) {
            return console.log(err);
                }
            
                console.log("The Customer Sign file was saved!");
             });
     
             var CustSignPath= CustSignPath.substring(1);
       
             var EmpSignData=header['EmpSign'];
             var data =EmpSignData;
            var EmpSignmatches = data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);           
            var EmpSignimageBuffer =new Buffer(EmpSignmatches[2], 'base64');
            var EmpSignName='Emp_'+header['EmpCd']+'_'+header['CompId']+'_'+
            '_'+header['SiteId']+'_'+  '_'+header['DocCd'].toString()+'_'+'_'+header['DocNo'].toString()+'_'+
            '_'+header['ItemNo'].toString()+'_'+CurrTime+'.png'
          
            var Empdir ='./Upload/30010/DocNo#'+header['DocNo'].toString()+'/';
            if (!fs.existsSync(Empdir)){
             fs.mkdirSync(Empdir);
             }
             var EmpSignPath=Empdir+EmpSignName;
            if (EmpSignmatches.length !== 3) {
                return new Error('Invalid input string');
              }
              fs.writeFile(EmpSignPath
              , EmpSignimageBuffer, function(err) {
                if(err) {
            return console.log(err);
                }
            
                console.log("The Employee Sign file was saved!");
             });
             var EmpSignPath= EmpSignPath.substring(1);
            const request = new sqlObj.Request(transactionObj);
            request
            .input('DocNo', sqlObj.BigInt, header['DocNo'])
            .input('SiteId', sqlObj.VarChar(10), header['SiteId'])
            .input('DocCd', sqlObj.Int, header['DocCd'])
            .input('CompId', sqlObj.VarChar(3), header['CompId'])
            .input('Revision', sqlObj.Int, header['Revision'])
            .input('ItemNo', sqlObj.Int,header['ItemNo'] )
            .input('JobCarriedOut', sqlObj.VarChar(100), header['JobCarriedOut'])
            .input('Remarks', sqlObj.VarChar(200), header['Remarks'])
            .input('CustSignPath', sqlObj.VarChar(500),CustSignPath)
            .input('CustSignedName', sqlObj.VarChar(100), header['CustSignedName'])
            .input('CustSignName', sqlObj.VarChar(500), CustSignName)
            .input('EmpSignPath', sqlObj.VarChar(500),EmpSignPath)
            .input('EmpSignedName', sqlObj.VarChar(100), header['EmpSignedName'])
            .input('EmpSignName', sqlObj.VarChar(500), EmpSignName)
                .execute('UpdateJobCardWorkSheetDet', (err, result) => {
                    if (!err) {
                        console.log('Update JobCardWorkSheetDet', result.recordset);
                        resolve(result.recordset);
                    } else {
                        console.error('JobCardWorkSheetDet log error ' + header['DocNo'] + '->', err);
                        reject(err);
                    }
                });
        }
        catch (error) {
            reject(error);
        }
    });
};

exports.UpdateSignature = async (req, res, next) => {
    try {
        let payLoad=req.body;
       
        let header=payLoad['Schedule'];
  
    
        var data =  header['sign'];
        var matches = data.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
       
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      
        var imageBuffer =new Buffer(matches[2], 'base64');
        fs.writeFile('./images/result.png', imageBuffer, function(err) {
            if(err) {
                return console.log(err);
            }
        
            console.log("The file was saved!");
         });
        console.log(imageBuffer);

        res.send({
            'Result':'Success'
        });

    } catch (error) {
        res.send(
            {
                status: false,
                data: [],
                reason: error.toString(),
            }
        );
    }
}

exports.getEmpTodaysSchedules = async (EmpCd, fromDt, toDt) => {
    return new Promise(async (resolve, reject) => {

        try {

            let con = await connectionObj.connect();
        
            let data = await con.query(`select  JOWD.CompId , JOWD.SiteId,JOWD.DocCd, 
            cast(JOWD.DocNo as int) DocNo,JOWD.Revision,JOWD.ItemNo,JOH.CustomerCd,
            ISNULL(JOH.CustomerCash,CM.CustomerDs) as CustomerDs,JOH.Type,JTM.JobTypeDs   JobType ,
            STM.ServiceTypeDs AS ServiceType ,WorkLatLong ,
                    JOH.CustAddress,JOH.Telephone,JOH.Remarks,JOH.JobNo,JOH.ContactDs,JOH.RefNo,JOH.JobDate
					,JOH.Fax,JOH.Mobile,JOH.StockCatCd as MachineCategory,JOH.RegNo,JOH.Description as Machine,JOH.Model as MachineItem   ,JOH.ModelDs  as MachineModelNo  
                    ,JOH.EngModal,JOH.EngSno,JOH.SerialNo,JOH.ServiceHrs,EmpCd,EmpDs ,WorkedOn,Duration ,InTime, 
                     OutTime, JOH.GrossTot,JOH.DiscountAmt,JOH.DiscountPerc,JOH.TotalAmount,JOH.JobStatus,JOH.CompletedDate,JOH.JobCompleted
                     from  WSJobOrderWorksheetDet  JOWD INNER JOIN WSJobOrderHdr JOH  
             ON JOWD.CompId=JOH.CompId AND    JOWD.SiteId  =JOH.SiteId  AND JOWD.DocCd=JOH.DocCd AND    JOWD.DocNo  =JOH.DocNo  AND    JOWD.Revision  =JOH.Revision 
             INNER JOIN CustomerMast CM ON JOH.CustomerCd=CM.CustomerCd
			 LEFT JOIN ServiceTypeMast STM ON JOH.CallType=STM.ServiceTypeCd  AND STM.Active=1
			 LEFT JOIN JobTypeMast JTM ON JOH.JobType= JTM.JobTypeCd   AND JTM.Active=1
             WHERE JOWD.EmpCd='`+EmpCd+`'  AND dbo.ufn_GetDateOnly(JOWD.WorkedOn)  between  dbo.ufn_GetDateOnly('` + fromDt + `' )  and  dbo.ufn_GetDateOnly('` + toDt + `' )   
              AND  ISNULL(JOH.JobStatus,'')<>'DON' AND ISNULL(JOH.JobCompleted,0)<>1`);

             
            console.log(data.recordset);

            resolve(data.recordset);

        } catch (e) {

            reject(e.toString());
        }

    });
};

exports.getTeamMembers = async (CompId,SiteId,DocCd,DocNo,Revision) => {
    return new Promise(async (resolve, reject) => {

        try {

            let con = await connectionObj.connect();
        
            let data = await con.query(`select EmpCd,EmpDs,InTime,OutTime,Duration from  WSJobOrderWorksheetDet   
            where CompId='`+CompId+`' AND    SiteId  ='`+SiteId+`'  AND DocCd=`+DocCd+`  AND    DocNo  =`+DocNo+`  
            AND    Revision  =`+Revision+`  AND dbo.ufn_GetDateOnly(WorkedOn)=dbo.ufn_GetDateOnly(GETDATE())`);

            console.log(data.recordset);

            resolve(data.recordset);

        } catch (e) {

            reject(e.toString());
        }

    });
};

exports.getMaterialInfo = async (CompId,SiteId,DocCd,DocNo,Revision) => {
    return new Promise(async (resolve, reject) => {

        try {

            let con = await connectionObj.connect();
        
            let data = await con.query(` select CompId,SiteId,DocCd,DocNo,Revision,ItemNo,DocDt,Code as ItemCode,Description,Type,Qty,Price,Unit,TotalPrice,TaxCd,TaxDs,TaxAmount,DiscPer,DiscAmt from  WSJobOrderEstDet  
            where CompId='`+CompId+`' AND    SiteId  ='`+SiteId+`'  AND DocCd=`+DocCd+`  AND    DocNo  =`+DocNo+`  
            AND    Revision  =`+Revision+``);

            console.log(data.recordset);

            resolve(data.recordset);

        } catch (e) {

            reject(e.toString());
        }

    });
};

exports.getTechnicalDiagnostics= async (CompId,SiteId,DocCd,DocNo,Revision) => {
    return new Promise(async (resolve, reject) => {

        try {

            let con = await connectionObj.connect();
        
            let data = await con.query(`   SELECT WSJOTDD.CompId,WSJOTDD.SiteId,WSJOTDD.DocCd,WSJOTDD.DocNo,WSJOTDD.Revision,WSJOTDD.ItemNo,WSJOTDD.DocDt ,WSJOTDD.DiagCd, D.JobComplaintDs, WSJOTDD.Completed
            , Remarks   FROM WSJobOrderTechDiagDet WSJOTDD 
           INNER JOIN JobComplaintMast D ON D.JobComplaintCd = WSJOTDD.DiagCd
           WHERE WSJOTDD.CompId = '`+CompId+`'
                                                    AND WSJOTDD.SiteId = '`+SiteId+`' 
                                                  AND WSJOTDD.DocCd=`+DocCd+`  AND    WSJOTDD.DocNo  =`+DocNo+`  AND    WSJOTDD.Revision  =`+Revision+`  
                                                  ORDER BY WSJOTDD.ItemNo ASC`);

            console.log(data.recordset);

            resolve(data.recordset);

        } catch (e) {

            reject(e.toString());
        }

    });
};


