
const sqlObj = require('mssql');

exports.getDocNo = (transactionObj, CompId, SiteId, DocCd) => {
    return new Promise(async (resolve, reject) => {
        try {

            const request = new sqlObj.Request(transactionObj);
            request.input('CompId', sqlObj.VarChar(3), CompId)
                .input('SiteId', sqlObj.VarChar(3), SiteId)
                .input('DocCd', sqlObj.Int, DocCd)
                .execute('GetDocumentNumber', (err, result) => {
                    if (!err) {
                     
                    } else {
                        console.error('sys log error ->', err);
                        reject(err);
                    }
                });
        }
        catch (error) {
            reject(error);
        }
    });
};

exports.createSysLog = async (TransactionObj, LogData) => {

    return new Promise(async (resolve, reject) => {
        try {

            const request = new sqlObj.Request(TransactionObj);

            request.input('SysTerminal', sqlObj.VarChar(9), LogData['SysTerminal'])
                .input('CompId', sqlObj.VarChar(3), LogData['CompId'])
                .input('SiteId', sqlObj.VarChar(3), LogData['SiteId'])
                .input('UserId', sqlObj.VarChar(5), LogData['UserId'])
                .input('LogAction', sqlObj.VarChar(10), LogData['LogAction'])
                .input('DocCd', sqlObj.Int, LogData['DocCd'])
                .input('DocKey', sqlObj.VarChar(2), LogData['DocKey'])
                .input('Description', sqlObj.NVarChar(4000), LogData['Description'])
                .execute('InsertSysLogMobile', (err, result) => {
                    if (!err) {
                        resolve(result);
                    } else {
                        console.error('sys log error ->', err);
                        reject(err);
                    }
                });

        } catch (e) {
            reject(e);
        }
    });

};